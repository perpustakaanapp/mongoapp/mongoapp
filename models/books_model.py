from mongoengine import Document, StringField, DynamicDocument, ObjectIdField,IntField
from bson.objectid import ObjectId

class books(Document):
    _id = ObjectIdField(required=True, default=ObjectId)
    nama = StringField(required=True, max_length=100)
    pengarang = StringField(required=True, max_length=200)
    tahunterbit = IntField(required=True, max_length=200)
    genre = StringField(required=True, max_length=50)
    def _init__(self,_id, nama, pengarang, tahunterbit,genre):
        self._id = _id
        self.nama=nama
        self.pengarang=pengarang
        self.tahunterbit=tahunterbit
        self.genre=genre
        