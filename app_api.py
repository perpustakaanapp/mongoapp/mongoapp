from controllers.books import Books
from bson.objectid import ObjectId

try:
    db = Books()
except Exception as e:
    print(e)

def objIdToStr(obj):
    return str(obj["_id"])

def search_book_by_name(**params):
    data_list = []
    for book in db.showBookByName(**params):
        dict_book = {
            "_id": objIdToStr(book),
            "nama": book.nama,
            "pengarang": book.pengarang,
            "tahunterbit": book.tahunterbit,
            "genre": book.genre
        }
        data_list.append(dict_book)
    return data_list

def search_books():
    data_list = []
    for book in db.showBooks():
        dict_book = {
            "_id": objIdToStr(book),
            "nama": book.nama,
            "pengarang": book.pengarang,
            "tahunterbit": book.tahunterbit,
            "genre": book.genre
        }
        data_list.append(dict_book)
    return data_list

def search_books_id(**params):
    data_list = []
    for book in db.showBookById(**params):
        dict_book = {
            "_id": objIdToStr(book),
            "nama": book.nama,
            "pengarang": book.pengarang,
            "tahunterbit": book.tahunterbit,
            "genre": book.genre
        }
        data_list.append(dict_book)
    return data_list