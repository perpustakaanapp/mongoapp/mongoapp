from mongoengine import connect
from mongoengine import Document, StringField, DynamicDocument

from models.books_model import books
import csv, json
import json
from bson.objectid import ObjectId

class Books:
    def __init__(self):
        try:
            self.db_uri = "mongodb://localhost:27017/perpustakaan"
            self.connection = connect(host=self.db_uri)
        except Exception as e:
            print(e)
        else:
            print("connected")
    
    def objIdToStr(obj):
        return str(obj["_id"])

    def showBooks(self):
        return [doc for doc in books.objects]

    def showBookById(self, **params):
        return [doc for doc in books.objects(_id=params["_id"])]

    def showBookByName(self, **params):
        return [doc for doc in books.objects(nama=params["nama"])]
           
    
    def insertBook(self, **params):
        books(**params).save()
    
    def updateBookById(self, **params):
        doc_1 = books.objects(_id=params["_id"]).first()
        doc_1.nama = params["nama"]
        doc_1.pengarang = params["pengarang"]
        doc_1.genre = params["genre"]
        doc_1.save()
    
    def deleteBookById(self, **params):
        doc_1 = books.objects(_id=params["_id"]).first()
        doc_1.delete()

    def closeConection(self):
        self.connection.close()  
